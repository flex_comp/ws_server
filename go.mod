module gitlab.com/flex_comp/ws_server

go 1.16

require (
	github.com/gobwas/ws v1.1.0
	gitlab.com/flex_comp/comp v0.1.3
	gitlab.com/flex_comp/conf v0.1.0
	gitlab.com/flex_comp/log v0.0.0-20210729190650-519dfabf348e
	gitlab.com/flex_comp/protobuf v0.0.0-20210730165512-8202d859a513
	gitlab.com/flex_comp/uid v0.0.0-20210729183622-f1ddefa50772
	gitlab.com/flex_comp/util v0.0.0-20210813015552-fa5a0e253e1a
	google.golang.org/protobuf v1.27.1
)
