package ws_server

import (
	"gitlab.com/flex_comp/log"
	"gitlab.com/flex_comp/protobuf"
	"google.golang.org/protobuf/proto"
	"net"
	"sync/atomic"
)

type WSClient struct {
	custom interface{}

	conn net.Conn

	sender chan []byte

	inactive     int32
	msgCounter   int32
	maxMsgPerSec int32

	done chan bool // 手动关闭
}

func NewWSClient(custom interface{}, conn net.Conn) *WSClient {
	return &WSClient{
		custom: custom,
		conn:   conn,
		sender: make(chan []byte, 1024),
		done:   make(chan bool),
	}
}

func (w *WSClient) Custom() interface{} {
	return w.custom
}

func (w *WSClient) Close() {
	go func() {
		select {
		case w.done <- true:
		default:
			log.Warn("通道阻塞")
		}
	}()
}

func (w *WSClient) RemoteAddr() net.Addr {
	return w.conn.RemoteAddr()
}

func (w *WSClient) incrMsgCount() bool {
	return atomic.AddInt32(&w.msgCounter, 1) < ins.maxMsgPerSec
}

func (w *WSClient) Send(seq uint64, msg proto.Message) {
	rsp := new(protobuf.Response)
	rsp.CmdNo = seq

	data, err := protobuf.MarshalRsp(rsp, msg)
	if err != nil {
		log.Error("序列化消息失败:", err)
		return
	}

	w.sender <- data
}

func (w *WSClient) SendWithClientIds(seq uint64, msg proto.Message, clientIds ...string) {
	rsp := new(protobuf.Response)
	rsp.CmdNo = seq

	switch len(clientIds) {
	case 0:
	case 1:
		rsp.ClientID = clientIds[0]
	default:
		rsp.ClientIDList = clientIds
	}

	data, err := protobuf.MarshalRsp(rsp, msg)
	if err != nil {
		log.Error("序列化消息失败:", err)
		return
	}

	w.sender <- data
}

func (w *WSClient) SendRaw(seq uint64, head string, msg []byte) {
	rsp := new(protobuf.Response)
	rsp.CmdNo = seq
	rsp.Head = head
	rsp.Data = msg

	data, err := protobuf.MarshalRspRaw(rsp)
	if err != nil {
		log.Error("序列化消息失败:", err)
		return
	}

	w.sender <- data
}

func (w *WSClient) SendRsp(rsp *protobuf.Response) {
	data, err := protobuf.MarshalRspRaw(rsp)
	if err != nil {
		log.Error("序列化消息失败:", err)
		return
	}

	w.sender <- data
}
