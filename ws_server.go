package ws_server

import (
	"fmt"
	"github.com/gobwas/ws"
	"github.com/gobwas/ws/wsutil"
	"gitlab.com/flex_comp/comp"
	"gitlab.com/flex_comp/conf"
	"gitlab.com/flex_comp/log"
	"gitlab.com/flex_comp/protobuf"
	"gitlab.com/flex_comp/uid"
	"gitlab.com/flex_comp/util"
	"google.golang.org/protobuf/proto"
	"math"
	"net"
	"net/http"
	"net/url"
	"sync"
	"sync/atomic"
	"time"
)

var (
	ins *WSServer
)

func init() {
	ins = &WSServer{
		clients:           make(map[interface{}]*WSClient),
		connMessage:       make(map[string]map[int64]chan *MsgInfo),
		unHandleMessage:   make(map[int64]chan *MsgInfo),
		chConn:            make(map[int64]chan *WSClient),
		chDisconn:         make(map[int64]chan *WSClient),
		listenAddrChanged: make(map[int64]chan string),
		alert:             make(map[int64]chan *Alert),
	}

	_ = comp.RegComp(ins)
}

type FnConnVerify func(addr net.IP, url *url.URL) (interface{}, bool)

type WSServer struct {
	open int32

	addr string

	connVerify    FnConnVerify
	connVerifyMtx sync.RWMutex

	clients    map[interface{}]*WSClient
	clientsMtx sync.RWMutex

	chConn    map[int64]chan *WSClient
	chConnMtx sync.RWMutex

	chDisconn    map[int64]chan *WSClient
	chDisconnMtx sync.RWMutex

	connMessage    map[string]map[int64]chan *MsgInfo // [head][uid]消息
	connMessageMtx sync.RWMutex

	unHandleMessage    map[int64]chan *MsgInfo // [uid]未处理消息
	unHandleMessageMtx sync.RWMutex

	listenAddrChanged    map[int64]chan string // [uid]监听地址变化
	listenAddrChangedMtx sync.RWMutex

	alert    map[int64]chan *Alert
	alertMtx sync.RWMutex

	maxMsgPerSec    int32
	inactiveTimeout int32
}

func (w *WSServer) Init(execArgs map[string]interface{}, _ ...interface{}) error {
	w.maxMsgPerSec = util.ToInt32(conf.Get("websocket.server.max_msg_per_sec"))
	if w.maxMsgPerSec == 0 {
		w.maxMsgPerSec = math.MaxInt32
	}

	w.inactiveTimeout = util.ToInt32(conf.Get("websocket.server.inactive_time_out"))

	go w.listen(execArgs)
	return nil
}

func (w *WSServer) Start(_ ...interface{}) error {
	return nil
}

func (w *WSServer) UnInit() {

}

func (w *WSServer) Name() string {
	return "ws-server"
}

func Open() {
	ins.Open()
}

func (w *WSServer) Open() {
	atomic.CompareAndSwapInt32(&w.open, 0, 1)
}

func Close() {
	ins.Open()
}

func (w *WSServer) Close() {
	atomic.CompareAndSwapInt32(&w.open, 1, 0)
}

func Address() string {
	return ins.Address()
}

func (w *WSServer) Address() string {
	return ins.addr
}

func OnConnVerify(fn FnConnVerify) {
	ins.OnConnVerify(fn)
}

func (w *WSServer) OnConnVerify(fn FnConnVerify) {
	w.connVerify = fn
}

func OnConnect() (chan *WSClient, int64) {
	return ins.OnConnect()
}

func (w *WSServer) OnConnect() (chan *WSClient, int64) {
	id := uid.Gen()
	ch := make(chan *WSClient, 1024)

	w.chConnMtx.Lock()
	w.chConn[id] = ch
	w.chConnMtx.Unlock()

	return ch, id
}

func OnDisconnect() (chan *WSClient, int64) {
	return ins.OnDisconnect()
}

func (w *WSServer) OnDisconnect() (chan *WSClient, int64) {
	id := uid.Gen()
	ch := make(chan *WSClient, 1024)

	w.chDisconnMtx.Lock()
	w.chDisconn[id] = ch
	w.chDisconnMtx.Unlock()

	return ch, id
}

func OnAddressChanged() (chan string, int64) {
	return ins.OnAddressChanged()
}

func (w *WSServer) OnAddressChanged() (chan string, int64) {
	id := uid.Gen()
	ch := make(chan string, 1024)

	w.listenAddrChangedMtx.Lock()
	w.listenAddrChanged[id] = ch
	w.listenAddrChangedMtx.Unlock()

	return ch, id
}

func OnAlert() (chan *Alert, int64) {
	return ins.OnAlert()
}

func (w *WSServer) OnAlert() (chan *Alert, int64) {
	id := uid.Gen()
	ch := make(chan *Alert, 1024)

	w.alertMtx.Lock()
	w.alert[id] = ch
	w.alertMtx.Unlock()

	return ch, id
}

func OnMessage(head string) (chan *MsgInfo, int64) {
	return ins.OnMessage(head)
}

func (w *WSServer) OnMessage(head string) (chan *MsgInfo, int64) {
	id := uid.Gen()
	ch := make(chan *MsgInfo, 1024)

	w.connMessageMtx.Lock()
	defer w.connMessageMtx.Unlock()

	m, ok := w.connMessage[head]
	if !ok {
		m = make(map[int64]chan *MsgInfo)
		w.connMessage[head] = m
	}

	m[id] = ch
	return ch, id
}

func OnUnHandleMessage() (chan *MsgInfo, int64) {
	return ins.OnUnHandleMessage()
}

func (w *WSServer) OnUnHandleMessage() (chan *MsgInfo, int64) {
	id := uid.Gen()
	ch := make(chan *MsgInfo, 10240)

	w.unHandleMessageMtx.Lock()
	defer w.unHandleMessageMtx.Unlock()

	w.unHandleMessage[id] = ch
	return ch, id
}

func GetClient(custom interface{}) *WSClient {
	return ins.GetClient(custom)
}

func (w *WSServer) GetClient(custom interface{}) *WSClient {
	w.clientsMtx.RLock()
	defer w.clientsMtx.RUnlock()

	cli := w.clients[custom]
	return cli
}

func Broadcast(seq uint64, msg proto.Message) {
	ins.Broadcast(seq, msg)
}

func (w *WSServer) Broadcast(seq uint64, msg proto.Message) {
	w.clientsMtx.RLock()
	defer w.clientsMtx.RUnlock()

	for _, client := range w.clients {
		client.Send(seq, msg)
	}
}

func BroadcastRaw(seq uint64, head string, msg []byte) {
	ins.BroadcastRaw(seq, head, msg)
}

func (w *WSServer) BroadcastRaw(seq uint64, head string, msg []byte) {
	w.clientsMtx.RLock()
	defer w.clientsMtx.RUnlock()

	for _, client := range w.clients {
		client.SendRaw(seq, head, msg)
	}
}

func BroadcastRsp(rsp *protobuf.Response) {
	ins.BroadcastRsp(rsp)
}

func (w *WSServer) BroadcastRsp(rsp *protobuf.Response) {
	w.clientsMtx.RLock()
	defer w.clientsMtx.RUnlock()

	for _, client := range w.clients {
		client.SendRsp(rsp)
	}
}

func Send(custom interface{}, seq uint64, msg proto.Message) {
	ins.Send(custom, seq, msg)
}

func (w *WSServer) Send(custom interface{}, seq uint64, msg proto.Message) {
	w.clientsMtx.RLock()
	defer w.clientsMtx.RUnlock()

	cli := w.clients[custom]
	if cli == nil {
		return
	}

	cli.Send(seq, msg)
}

func SendWithClientIds(custom interface{}, seq uint64, msg proto.Message, clientIds ...string) {
	ins.SendWithClientIds(custom, seq, msg, clientIds...)
}

func (w *WSServer) SendWithClientIds(custom interface{}, seq uint64, msg proto.Message, clientIds ...string) {
	w.clientsMtx.RLock()
	defer w.clientsMtx.RUnlock()

	cli := w.clients[custom]
	if cli == nil {
		return
	}

	cli.SendWithClientIds(seq, msg, clientIds...)
}

func SendRaw(custom interface{}, seq uint64, head string, msg []byte) {
	ins.SendRaw(custom, seq, head, msg)
}

func (w *WSServer) SendRaw(custom interface{}, seq uint64, head string, msg []byte) {
	w.clientsMtx.RLock()
	defer w.clientsMtx.RUnlock()

	cli := w.clients[custom]
	if cli == nil {
		return
	}

	cli.SendRaw(seq, head, msg)
}

func SendRsp(custom interface{}, rsp *protobuf.Response) {
	ins.SendRsp(custom, rsp)
}

func (w *WSServer) SendRsp(custom interface{}, rsp *protobuf.Response) {
	w.clientsMtx.RLock()
	defer w.clientsMtx.RUnlock()

	cli := w.clients[custom]
	if cli == nil {
		return
	}

	cli.SendRsp(rsp)
}

func (w *WSServer) listen(execArgs map[string]interface{}) {
	var (
		host    string
		minPort int
		maxPort int
		secure  bool
	)

	host = util.ToString(conf.Get("websocket.server.host"))
	for i := 0; i < 1; i++ {
		if len(host) != 0 {
			break
		}

		if execArgs == nil {
			break
		}

		if v, ok := execArgs["host"]; ok {
			host = util.ToString(v)
		}
	}

	minPort = util.ToInt(conf.Get("websocket.server.min_port"))
	maxPort = util.ToInt(conf.Get("websocket.server.max_port"))
	secure = util.ToBool(conf.Get("websocket.server.secure"))

	curPort := minPort

RETRY:
	addr := fmt.Sprintf("%s:%d", host, curPort)
	log.Info("尝试监听:", addr)

	ln, err := net.Listen("tcp", addr)
	if err != nil {
		log.Warn("监听失败:", addr, ". ", err)
		if curPort < maxPort {
			curPort++
			goto RETRY
		}

		log.Warn("监听端口范围已用尽, 5秒后从最小端口重新开始")
		<-time.After(time.Second * 5)

		curPort = minPort
		goto RETRY
	}

	w.listenAddrChangedMtx.RLock()
	w.addr = fmt.Sprintf("%s", ln.Addr().String())
	for _, c := range w.listenAddrChanged {
		c <- w.addr
	}
	w.listenAddrChangedMtx.RUnlock()

	log.Info("监听完成:", ln.Addr().String())
	srv := http.Server{
		Addr:    ln.Addr().String(),
		Handler: http.HandlerFunc(w.onServer),
	}

	if secure {
		cert := util.ToString(conf.Get("websocket.server.cert"))
		key := util.ToString(conf.Get("websocket.server.key"))

		err = srv.ServeTLS(ln, cert, key)
	} else {
		err = srv.Serve(ln)
	}

	if err != nil {
		w.listenAddrChangedMtx.RLock()
		w.addr = ""
		for _, c := range w.listenAddrChanged {
			c <- w.addr
		}
		w.listenAddrChangedMtx.RUnlock()

		log.Warn("监听失败:", addr, ". ", err)
		if curPort < maxPort {
			curPort++
			goto RETRY
		}

		log.Warn("监听端口范围已用尽, 5秒后从最小端口重新开始")
		<-time.After(time.Second * 5)

		curPort = minPort
		goto RETRY
	}
}

func (w *WSServer) onServer(rsp http.ResponseWriter, req *http.Request) {
	w.clientsMtx.Lock()
	defer w.clientsMtx.Unlock()

	var custom interface{}
	if w.connVerify != nil {
		ip := net.ParseIP(util.ParseIPByAddr(req.RemoteAddr))

		w.connVerifyMtx.RLock()
		var ok bool
		custom, ok = w.connVerify(ip, req.URL)
		w.connVerifyMtx.RUnlock()

		if !ok {
			w.sendAlert(ATVerifyFailed, ip.String())
			rsp.WriteHeader(http.StatusUnauthorized)
			return
		}
	}

	conn, _, _, err := ws.UpgradeHTTP(req, rsp)
	if err != nil {
		rsp.WriteHeader(http.StatusInternalServerError)
		log.Error("创建WS连接失败: ", err)
		return
	}

	cli := NewWSClient(custom, conn)

	old := w.clients[custom]
	w.clients[custom] = cli

	if old != nil {
		old.Close()
	}

	chClose := make(chan bool, 1)
	go w.writePump(cli, chClose)
	go w.readPump(cli, chClose)

	w.chConnMtx.RLock()
	defer w.chConnMtx.RUnlock()

	for _, ch := range w.chConn {
		select {
		case ch <- cli:
		default:
			log.Warn("通道阻塞")
		}
	}
}

func (w *WSServer) disconnect(cli *WSClient) {
	w.clientsMtx.Lock()
	defer w.clientsMtx.Unlock()

	delete(w.clients, cli.Custom())

	w.chConnMtx.RLock()
	defer w.chConnMtx.RUnlock()

	for _, ch := range w.chDisconn {
		select {
		case ch <- cli:
		default:
			log.Warn("通道阻塞")
		}
	}
}

func (w *WSServer) sendAlert(ty AlertType, ip string) {
	a := &Alert{
		Ty: ty,
		IP: ip,
	}

	w.alertMtx.RLock()
	for _, ch := range w.alert {
		select {
		case ch <- a:
		default:
			log.Warn("通道阻塞")
		}
	}

	w.alertMtx.RUnlock()
}

func (w *WSServer) readPump(cli *WSClient, chClose chan<- bool) {
	defer func() {
		select {
		case chClose <- true:
		default:
		}
	}()

	if w.inactiveTimeout > 0 {
		go func() {
			<-time.After(time.Millisecond * time.Duration(w.inactiveTimeout))
			if atomic.LoadInt32(&cli.inactive) == 0 {
				w.sendAlert(ATInactive, util.ParseIPByAddr(cli.conn.RemoteAddr().String()))
			}
		}()
	}

	for {
		data, op, err := wsutil.ReadClientData(cli.conn)
		if err != nil {
			log.Warn("Websocket数据读取错误, 断开连接, Address:", cli.conn.RemoteAddr(), " 错误:", err)
			return
		}

		switch op {
		case ws.OpClose:
			return
		case ws.OpText, ws.OpPing, ws.OpPong:
			continue
		}

		// 消息计数
		if !cli.incrMsgCount() {
			w.sendAlert(ATMsgTooQuick, util.ParseIPByAddr(cli.conn.RemoteAddr().String()))
			return
		}

		// 从客户端获取的Client ID在连接时已验证,忽略消息内信息
		req, pkg, err := protobuf.UnMarshalReq(data)
		if err != nil {
			log.Error("Websocket数据解析错误, Address:", cli.conn.RemoteAddr(), " 错误:", err)
			// 错误消息Alert
			w.sendAlert(ATMsgInvalid, util.ParseIPByAddr(cli.conn.RemoteAddr().String()))
			continue
		}

		// 不活动连接标记清理
		atomic.CompareAndSwapInt32(&cli.inactive, 0, 1)

		mi := &MsgInfo{
			Cli: cli,
			Req: req,
			Pkg: pkg,
		}

		w.connMessageMtx.RLock()
		m, ok := w.connMessage[req.Head]
		if !ok {
			// 消息未被监听处理, 直接任未处理中
			w.unHandleMessageMtx.RLock()
			for _, ch := range w.unHandleMessage {
				select {
				case ch <- mi:
				default:
					log.Warn("UnHandle 消息 push 失败")
				}
			}
			w.unHandleMessageMtx.RUnlock()
		} else {
			for _, ch := range m {
				select {
				case ch <- mi:
				default:
					log.Warn("消息 push 失败")
				}
			}
		}

		w.connMessageMtx.RUnlock()
	}
}

func (w *WSServer) writePump(cli *WSClient, chClose <-chan bool) {
	ticker := time.NewTicker(time.Second)
	defer func() {
		ticker.Stop()
		w.disconnect(cli)
		_ = cli.conn.Close()
	}()

	for {
		select {
		case <-ticker.C:
			atomic.StoreInt32(&cli.msgCounter, 0)
		case <-cli.done:
			return
		case <-chClose:
			return
		case raw := <-cli.sender:
			err := wsutil.WriteServerMessage(cli.conn, ws.OpBinary, raw)
			if err != nil {
				log.Error("发送消息失败:", err)
				return
			}
		}
	}
}
