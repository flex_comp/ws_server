package ws_server

type AlertType int

const (
	ATUnknown AlertType = iota
	ATInactive
	ATVerifyFailed
	ATMsgTooQuick
	ATMsgInvalid
)

type Alert struct {
	Ty AlertType
	IP string
}
