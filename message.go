package ws_server

import (
	"gitlab.com/flex_comp/protobuf"
	"google.golang.org/protobuf/proto"
)

type MsgInfo struct {
	Cli *WSClient
	Req *protobuf.Request
	Pkg proto.Message
}
